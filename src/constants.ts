export const DEFAULT_CITIES: ICity[] = [
  { id: "amsterdam", name: "Amsterdam", lat: 52.3667, long: 4.8945 },
  { id: "leer", name: "Leer", lat: 53.2357, long: 7.4679 },
  { id: "bremen", name: "Bremen", lat: 53.073635, long: 8.806422 },
  { id: "buffalo", name: "Buffalo", lat: 42.8864, long: -78.8784 },
  { id: "vienna", name: "Vienna", lat: 48.2082, long: 16.3738 },
  { id: "tokyo", name: "Tokyo", lat: 35.6762, long: 139.6503 },
  { id: "bangkok", name: "Bangkok", lat: 13.7563, long: 100.5018 }
];

export const FORECAST_APP_ID = "dceb26ef7cc4fc3d84ac692b3d85adec";
export const NUMBER_OF_DAYS = 3;
