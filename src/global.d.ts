interface ICity {
  id: string;
  name: string;
  lat: number;
  long: number;
}
